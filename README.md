# MoviesLuca

El ejercicio consiste en construir una pequeña aplicación utilizando cualquier Front-End, (Angular preferiblemente) donde se muestre un listado de películas con acceso a una pantalla de detalle.

### Para ello será necesario que:
- Te registres en algún servicio online donde puedas realizar llamadas a un API de películas. Puedes crear tu propia api o Por ejemplo puedes utilizar The Movie Database (https://www.themoviedb.org/documentation/api) o The Open Movie Database (https://www.omdbapi.com).
- Utilizando los servicios de dicho API crees una pequeña interfaz con dos pantallas: Listado de Películas y Detalle de Película.
- El listado de películas mostraría simplemente el nombre de las películas de una búsqueda cualquiera que tú mismo decidas (p.e. películas que tengan un nombre concreto o que sean de un cierto país / idioma, eso como quieras).
- Desde el listado de películas (que sólo son los nombres) al seleccionar una se mostraría el detalle de la misma: nombre, cartel, fecha, idioma, descripción y cualquier otro campo que quieras adicionalmente.